package com.example.restoran;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class TampilActivity extends AppCompatActivity {
    private String tanggal, jam, noMeja, kodeMenu, harga, jumlah;
    private TextView txtTanggal, txtJam, txtNoMeja, txtKodeMenu, txtHarga, txtMenu, txtJumlah;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tampil);

        Intent intent = getIntent();

        tanggal = intent.getStringExtra(konfigurasi.EMP_TANGGAL);
        jam = intent.getStringExtra(konfigurasi.EMP_JAM);
        noMeja = intent.getStringExtra(konfigurasi.EMP_NOMEJA);
        kodeMenu = intent.getStringExtra(konfigurasi.EMP_KODEMENU);
        harga = intent.getStringExtra(konfigurasi.EMP_HARGA);
        jumlah = intent.getStringExtra(konfigurasi.EMP_JUMLAH);


        txtTanggal = (TextView) findViewById(R.id.tanggal);
        txtJam = (TextView) findViewById(R.id.jam);
        txtNoMeja = (TextView) findViewById(R.id.noMeja);
        txtKodeMenu = (TextView) findViewById(R.id.kodeMenu);
        txtHarga = (TextView) findViewById(R.id.harga);
        txtMenu = (TextView) findViewById(R.id.menu);
        txtJumlah = (TextView) findViewById(R.id.jumlah);



        if(intent.getStringExtra(konfigurasi.EMP_KODEMENU).equals("B01")){
            txtMenu.setText("Minuman : Kopi Hitam ");
        }if(intent.getStringExtra(konfigurasi.EMP_KODEMENU).equals("B02")){
            txtMenu.setText("Minuman : Cappuccino \nPaduan Kopi dengan buih susu kental serta karamel");
        }if(intent.getStringExtra(konfigurasi.EMP_KODEMENU).equals("M03")){
            txtMenu.setText("Minuman : Sparkling Tea \nMinuman Teh yang menggunakan daun teh dari pegunungan");
        }if(intent.getStringExtra(konfigurasi.EMP_KODEMENU).equals("F01")){
            txtMenu.setText("Makanan : Batagor \nBaso dan Tahu goreng dengan sajian bumbu kacang dan kecap");
        }if(intent.getStringExtra(konfigurasi.EMP_KODEMENU).equals("F02")){
            txtMenu.setText("Makanan : Cireng \nMakanan ringan berupa tepung kanji goreng");
        }

        txtTanggal.setText(tanggal);
        txtJam.setText(jam);
        txtNoMeja.setText(noMeja);
        txtKodeMenu.setText(kodeMenu);
        txtJumlah.setText(jumlah);
        double jum = Double.parseDouble(jumlah);
        double har = Double.parseDouble(harga);
        double result = jum * har;
        txtHarga.setText(Double.toString(result));


        Button home = findViewById(R.id.home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent openPageIntent = new Intent(TampilActivity.this, HomeActivity.class);
                startActivity(openPageIntent);
            }
        });
    }
}
