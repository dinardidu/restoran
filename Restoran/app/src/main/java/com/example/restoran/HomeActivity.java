package com.example.restoran;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Button openPage = findViewById(R.id.tambah);
        openPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent openPageIntent = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(openPageIntent);
            }
        });

        Button menu = findViewById(R.id.order);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent openPageIntent = new Intent(HomeActivity.this, MenuCafe.class);
                startActivity(openPageIntent);
            }
        });
    }
}
