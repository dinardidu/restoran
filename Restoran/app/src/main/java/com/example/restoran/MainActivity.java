package com.example.restoran;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity  {
    private EditText editTanggal;
    private EditText editJam;
    private EditText editNoMeja;
    private EditText editKodeMenu;
    private EditText editHarga;
    private EditText editJumlah;

    private Button tambah;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTanggal = (EditText) findViewById(R.id.tanggal);
        editJam = (EditText) findViewById(R.id.jam);
        editNoMeja = (EditText) findViewById(R.id.noMeja);
        editKodeMenu = (EditText) findViewById(R.id.kodeMenu);
        editHarga = (EditText) findViewById(R.id.harga);
        editJumlah = (EditText) findViewById(R.id.jumlah);

        tambah = (Button) findViewById(R.id.tambah);

        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent openPageIntent = new Intent(MainActivity.this, TampilActivity.class);
                final String tanggal = editTanggal.getText().toString().trim();
                final String jam = editJam.getText().toString().trim();
                final String noMeja = editNoMeja.getText().toString().trim();
                final String kodeMenu = editKodeMenu.getText().toString().trim();
                final String harga = editHarga.getText().toString().trim();
                final String jumlah = editJumlah.getText().toString().trim();
                openPageIntent.putExtra(konfigurasi.EMP_TANGGAL, tanggal);
                openPageIntent.putExtra(konfigurasi.EMP_JAM, jam);
                openPageIntent.putExtra(konfigurasi.EMP_NOMEJA, noMeja);
                openPageIntent.putExtra(konfigurasi.EMP_KODEMENU,kodeMenu);
                openPageIntent.putExtra(konfigurasi.EMP_HARGA,harga);
                openPageIntent.putExtra(konfigurasi.EMP_JUMLAH,jumlah);
                startActivity(openPageIntent);
            }
        });
    }


}
